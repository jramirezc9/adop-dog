// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCjOkNe9FCjU1fRoSPCASevAtDnCpPS6-Y",
    authDomain: "adop-dog.firebaseapp.com",
    projectId: "adop-dog",
    storageBucket: "adop-dog.appspot.com",
    messagingSenderId: "1012521603651",
    appId: "1:1012521603651:web:d444a9eb46e688a75f7af6",
    measurementId: "G-7D3R21LR6H"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
