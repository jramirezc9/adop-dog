import { Injectable } from '@angular/core';
import { Login } from './login.interface';
import * as firebase from 'firebase/auth'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  loginUser(login: Login): Promise<firebase.UserCredential> {
    return firebase.signInWithEmailAndPassword(firebase.getAuth(),login.email, login.password);
  }

  signUpUser(login: Login) : Promise<any>{
    return firebase.createUserWithEmailAndPassword(firebase.getAuth(), login.email, login.password);
  }

  resetPassword(email: string) : Promise<void>{
    return firebase.sendPasswordResetEmail(firebase.getAuth(), email);
  }

  logOutUser(): Promise<void>{
    return firebase.signOut(firebase.getAuth());
  }
}
