export interface Request {
    id?: string|null;
    birthday?: Date|null;
    department?: string|null;
    municipality?: string|null;
    address?: string|null;
    phone?: string|null;
    references?: string|null;
    reason?: string|null;
    dpi?: string|null;
    personFullName?: string|null,
    personId?: string|null;
    ownerPostFullName?: string|null;
    ownderPostId?: string|null;
    postId?: string|null;
    status?: string|null;
    createdAt?: Date|null;
    createdBy?: string|null;
    updatedAt?: Date|null;
    updatedBy?: string|null;
}