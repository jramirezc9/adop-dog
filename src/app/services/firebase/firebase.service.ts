import { Injectable } from '@angular/core';
import * as firebase from 'firebase/firestore'
import { Firestore, query, where } from '@firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  db: Firestore
  constructor() {
    this.db = firebase.getFirestore();
  }

  public async firestoreInstance() {
    return {
      db: this.db,
      firebase: firebase
    }
  }

  public async getDeparments() {
    return firebase.getDocs(query(firebase.collection(this.db, `department`)))
  }

  public async getMunicipalities(departmentId: string) {
    return firebase.getDocs(query(firebase.collection(this.db, `department/${departmentId}/municipality`)))
  }

  public async getRaces() {
    return firebase.getDocs(query(firebase.collection(this.db, `race`)))
  }
}
