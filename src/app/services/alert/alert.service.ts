import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(public alertController: AlertController) { }

  async presentAlertWarning(title: string, message: string) {
    const alert = await this.alertController.create({
      cssClass: 'alertWarning',
      header: 'Alerta',
      subHeader: `${title}`,
      message: `${message}`,
      buttons: ['Aceptar']
    });

    await alert.present();
  }

  async presentAlertInfo(title: string, message: string) {
    const alert = await this.alertController.create({
      cssClass: 'alertInfo',
      header: 'Alerta',
      subHeader: `${title}`,
      message: `${message}`,
      buttons: ['Aceptar']
    });

    await alert.present();
  }

  async presentAlertError(title: string, message: string) {
    const alert = await this.alertController.create({
      cssClass: 'alertError',
      header: 'Alerta',
      subHeader: `${title}`,
      message: `${message}`,
      buttons: ['Aceptar']
    });

    await alert.present();
  }

  async presentAlertConfirm(message: string, fn: Function) {
    const alert = await this.alertController.create({
      cssClass: 'alertWarning',
      header: 'Confirmación',
      message: `<strong>${message}</strong>`,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            return false;
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            fn();
            return true;
          }
        }
      ]
    });

    await alert.present();
  }
}
