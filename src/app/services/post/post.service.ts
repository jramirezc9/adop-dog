import { Injectable } from '@angular/core';
import { query, where } from '@firebase/firestore';
import { FirebaseService } from '../firebase/firebase.service';
import { Post } from './post.interface';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private firestoreService: FirebaseService
  ) { }

  public async getPosts(status: string) {
    return (await this.firestoreService.firestoreInstance()).
    firebase.getDocs(query((await this.firestoreService.firestoreInstance())
    .firebase.collection(this.firestoreService.db, `post`), where('status', '==', status)))
  }

  public async getPost(postId: string) {
    return (await this.firestoreService.firestoreInstance()).firebase.getDoc(
      (await this.firestoreService.firestoreInstance())
      .firebase.doc(this.firestoreService.db, `post/${postId}`))
  }

  async updatePost(status: string, postId: string) : Promise<any> {
    try {
      const docRef = await (await this.firestoreService.firestoreInstance())
      .firebase.doc(this.firestoreService.db, `post/${postId}`)

      return (await this.firestoreService.firestoreInstance())
      .firebase.updateDoc(docRef, {
        status: status
      });   

    } catch (e) {
      console.error("Error adding document: ", e);
    }
  }

  async savePost(post: Post): Promise<string> {
    try {
      const docRef = await (await this.firestoreService.firestoreInstance())
      .firebase.addDoc((await this.firestoreService.firestoreInstance())
      .firebase.collection(this.firestoreService.db, "post"), post);
    
      console.log("Document written with ID: ", docRef.id);
      return docRef.id
    } catch (e) {
      console.error("Error adding document: ", e);
    }
  }
}
