import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AlertService } from '../services/alert/alert.service';
import { Post } from '../services/post/post.interface';
import { PostService } from '../services/post/post.service';
//import { FarmingGuide } from '../services/firebase/farmingGuide.interface';
import { FirebaseService } from '../services/firebase/firebase.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  userId: string;
  posts: Post[];
  constructor(
    private menu: MenuController,
    private postService: PostService,
    private firebaseService: FirebaseService,
    private alert: AlertService
  ) {
    this.posts = [] as Post[]
    this.userId = window.localStorage.getItem('userId');
  }

  ngOnInit() {
  }

  async ionViewDidEnter() {
    this.posts = [] as Post[]
    console.log('entro getPosts')
    return await this.postService.getPosts('Activo')
    .then(data => {
      console.log('data firebase posts: ', data)
      data.forEach(async (doc) => {
        let post: Post = doc.data() as Post
        post.id = doc.id;
        console.log(`${doc.id} => ${doc.data().name}`);
        //area.id = doc.id;
        //let farmerGuide = await (await this.firebaseService.getFarmingGuide(area.protocolId))
        //area.farmerGuide = farmerGuide.data() as FarmingGuide;
        this.posts.push(post as Post)
      })
      
      console.log(`post list: `, this.posts);
    }, async error => {
      await this.alert.presentAlertError('Error', error.message);
    })
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

}
