import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from '../services/post/post.interface';
import { Request } from '../services/request/request.interface';
import { FirebaseService } from '../services/firebase/firebase.service';
import { RequestService } from '../services/request/request.service';
import { AlertService } from '../services/alert/alert.service';
import { PostService } from '../services/post/post.service';
import { ImageService } from '../services/image/image.service';

@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.page.html',
  styleUrls: ['./create-request.page.scss'],
})
export class CreateRequestPage implements OnInit {
  request: Request;
  userId: string;
  postId: string;
  departments: {id: string; department: string}[]
  municipalities: {id: string; municipality: string; department: string}[]
  constructor(
    private activateRouter: ActivatedRoute,
    private firebase: FirebaseService,
    private postService: PostService,
    private requestService: RequestService,
    private imageService: ImageService,
    private alert: AlertService,
    private router: Router
  ) {
    this.request = {} as Request;
    this.departments = [];
    this.municipalities = [];
    this.userId = window.localStorage.getItem('userId');
    this.postId = this.activateRouter.snapshot.paramMap.get('postId');
  }

  async ngOnInit() {
    await this.firebase.getDeparments()
    .then(data => {
      console.log('data firebase department: ', data)
      data.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data().department}`);
        this.departments.push({
          id: doc.id,
          department: doc.data().department
        })
      })
    }, async error => {
      await this.alert.presentAlertError('Error', error.message);
    })
  }

  async getMunicipalities(departmentId: string) {
    this.municipalities = [];
    this.request.municipality = null;
    this.firebase.getMunicipalities(departmentId)
    .then(data => {
      console.log('data firebase municipality: ', data)
      data.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data().municipality}`);
        this.municipalities.push({
          id: doc.id,
          municipality: doc.data().municipality,
          department: doc.data().department,
        })
      })
    }, async error => {
      await this.alert.presentAlertError('Error', error.message);
    })
  }

  async save() {
    this.request.createdAt = new Date();
    this.request.updatedAt = new Date();
    this.request.createdBy = this.userId;
    this.request.updatedBy = this.userId;
    this.request.postId = this.postId;
    const postGot =  await this.postService.getPost(this.postId);
    this.request.ownderPostId = postGot.data().personId;
    this.request.status = 'Enviada';
    console.log('esto lleva request: ', this.request);
    this.requestService.saveRequest(this.request)
    .then(async (data) => {
      console.log('registro request guardado: ', data)
      return this.alert.presentAlertInfo('Estado del registro', 'La solicitud fue enviada')
      .then(() => {
        return this.router.navigateByUrl('home');
      })
    }, async error => {
      await this.alert.presentAlertError('Error', error.message);
    })
  }

  takePhoto() {
    this.imageService.takePhoto()()
    .then((data: string) => {
      this.request.dpi = data ? data : null;
      console.log('foto de camara: ', this.request.dpi);
    })
    .catch( async (error) => {
      await this.alert.presentAlertError('Error', error.message);
    });
  }

}
