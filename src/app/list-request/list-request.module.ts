import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListRequestPageRoutingModule } from './list-request-routing.module';

import { ListRequestPage } from './list-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListRequestPageRoutingModule
  ],
  declarations: [ListRequestPage]
})
export class ListRequestPageModule {}
