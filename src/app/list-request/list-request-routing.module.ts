import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListRequestPage } from './list-request.page';

const routes: Routes = [
  {
    path: '',
    component: ListRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListRequestPageRoutingModule {}
