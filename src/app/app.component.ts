import { Component } from '@angular/core';
import * as firebase from 'firebase/app';
import { environment } from 'src/environments/environment';
import { MenuController, Platform } from '@ionic/angular';
import { AlertService } from './services/alert/alert.service';
import { AuthService } from './services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Mis solicitudes', url: '/list-request/mine', icon: 'mail' },
    { title: 'Bandeja de solicitudes', url: '/list-request/got', icon: 'paper-plane' },
    /*{ title: 'Inbox', url: '/folder/Inbox', icon: 'mail' },
    { title: 'Outbox', url: '/folder/Outbox', icon: 'paper-plane' },
    { title: 'Favorites', url: '/folder/Favorites', icon: 'heart' },
    { title: 'Archived', url: '/folder/Archived', icon: 'archive' },
    { title: 'Trash', url: '/folder/Trash', icon: 'trash' },
    { title: 'Spam', url: '/folder/Spam', icon: 'warning' },*/
  ];
  public labels = ['Cerrar sesión'];
  constructor(
    private platform: Platform,
    private authService: AuthService,
    private alert: AlertService,
    private router: Router,
    private menu: MenuController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

    })

    firebase.initializeApp(environment.firebaseConfig);
  }

  async logOut() : Promise<void>{
    this.authService.logOutUser()
    .then((data) => {
      console.log('cerrar sesión: ', data)
      this.closeMenu();
      this.router.navigateByUrl('login');
    },
    async error => {
      await this.alert.presentAlertError('Error', "Ocurrió un error al cerrar sesión");
    })
  }

  closeMenu() {
    this.menu.enable(true, 'first');
    this.menu.close('first');
  }
}
